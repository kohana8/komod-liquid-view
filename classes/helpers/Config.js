const loadSettings = (themePath) => {
  if(!themePath)return {current: {}};

  const config = require(themePath + 'config/settings_data.json');
  if(!config)return {current: {}};
  if(config.current === "Default") {
    config.current = config.presets.Default;
  }
  return config;
};

const loadSectionSettings = (themePath, sectionName) => {
  if(!themePath)return {};

  const config = loadSettings(themePath);
  config.current.sections[sectionName] = config.current.sections[sectionName] || {};

  //get the section setting
  return config.current.sections[sectionName].settings;
};

module.exports = {
  loadSectionSettings : loadSectionSettings,
  loadSettings: loadSettings
};