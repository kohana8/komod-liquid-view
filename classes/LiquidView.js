const fs = require('fs');
const {K8, View} = require('@komino/k8');
const {Liquid} = require('liquidjs');

const HelperConfig = K8.require('helpers/Config');
const HelperLiquid = K8.require('helpers/Liquid');
const TagSection = K8.require('liquid-tags/Section');

class LiquidView extends View{
  constructor(file, data={}, themePath=null){
    file = file + '.liquid';
    super(file, data);

    if( !themePath ){
      const fetchedView = K8.resolveView(file);
      themePath = fetchedView.replace(file, '');
    }

    this.realPath = themePath + file;

    //load settings
    const settings = HelperConfig.loadSettings(this.themePath, this.sectionFile);
    Object.assign(this.data, {settings: settings.current});

    if( !View.caches[this.realPath] ) {
      const engine = new Liquid({
        root: themePath + 'snippets',
        extname: '.liquid',
        cache: K8.config.cache.view,
        globals: this.data,
      });

      engine.registerTag('section', new TagSection(themePath));
      HelperLiquid.registerTags(engine);

      View.caches[this.realPath] = {
        engine: engine,
        template: engine.parse(fs.readFileSync(this.realPath, 'utf8'))
      };
    }
  }

  async render(){
    const engine   = LiquidView.caches[this.realPath].engine;
    const template = LiquidView.caches[this.realPath].template;
    return await engine.render(template, this.data || this.collectProps());
  }
}

module.exports = LiquidView;