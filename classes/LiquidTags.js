/**
 *
 * tagToken:

 "trimLeft":true,
 "trimRight":true,
 "type" : "tag",
 "raw" : "{%- form 'product', _product, class : form_classes, novalidate: 'novalidate' -%}",
 "value" : "form 'product', _product, class : form_classes, novalidate: 'novalidate'",
 "input" : ,
 "file" : "xxx.liquid",
 "name" : "form",
 "args" : "'product', _product, class : form_classes, novalidate: 'novalidate'"

 */

const {K8} = require('@komino/k8');
const paginate = K8.require('liquid-tags/Paginate');
const form = K8.require('liquid-tags/Form');
const stub = K8.require('liquid-tags/Stub');
const tag  = K8.require('liquid-tags/Tag');
const style = K8.require('liquid-tags/Style');

module.exports = {
    paginate : paginate,
    form: form,
    stub : stub,
    tag : tag,
    style : style,
};