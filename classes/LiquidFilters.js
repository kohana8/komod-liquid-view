/**
 // Usage: {{ 1 | add: 2, 3 }}
 engine.registerFilter('add', (initial, arg1, arg2) => initial + arg1 + arg2)
 */

const camelCase = str => str
    .replace(/\s(.)/g, $1 => $1.toUpperCase())
    .replace(/\s/g, '')
    .replace(/^(.)/, $1 => $1.toUpperCase());


module.exports = {
    t              : v => `t(${v})`,
    money          : v => new Intl.NumberFormat('en', {style: 'currency', currency: 'HKD'}).format(v),
    moneyWithoutCurrency : v => new Intl.NumberFormat('en', {style: 'decimal'}).format(v),
    camelcase      : camelCase,
    script_tag     : v => `<script src="${v}" type="text/javascript"></script>`,
    stylesheet_tag : v => `<link type="text/css" href="${v}" rel="stylesheet"/>`,
    asset_url      : v => `/assets/${v}`,
    shopify_asset_url : v => `//cdn.shopify.com/s/shopify/${v}`,
    json           : v => `${JSON.stringify(v)}`,
    if             : (v, arg1, arg2) =>  v ? arg1 : arg2,
};