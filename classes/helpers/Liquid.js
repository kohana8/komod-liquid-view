const {K8} = require('@komino/k8');
const Tags = K8.require('LiquidTags');
const Filters = K8.require('LiquidFilters');

class HelperLiquid{
    static registerTags(engine){
        engine.registerFilter('t',              Filters.t);
        engine.registerFilter('asset_url',      Filters.asset_url);
        engine.registerFilter('shopify_asset_url', Filters.shopify_asset_url);
        engine.registerFilter('stylesheet_tag', Filters.stylesheet_tag);
        engine.registerFilter('script_tag',     Filters.script_tag);
        engine.registerFilter('camelcase',      Filters.camelcase);
        engine.registerFilter('money',          Filters.money);
        engine.registerFilter('money_without_currency', Filters.moneyWithoutCurrency);
        engine.registerFilter('json',           Filters.json);
        engine.registerFilter('if',             Filters.if);

        // Usage: {% upper name%}
        engine.registerTag('style',       new Tags.style());
        engine.registerTag('stylesheet',  new Tags.style());
        engine.registerTag('javascript',  Tags.tag);

        engine.registerTag('form',        Tags.form);
        engine.registerTag('paginate',    Tags.paginate);
    }
}

module.exports = HelperLiquid;