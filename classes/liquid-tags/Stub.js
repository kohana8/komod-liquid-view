class StubTag{
    parse(token){
        this.token = token;
        this.tagName = this.token.name;
    }

    *render(ctx, emitter){
        emitter.write(`:: ${this.tagName} :: `);
    }
}

module.exports = StubTag;